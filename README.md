# RedUnlocker
## Description
These GM9 scripts will unlock the red permission of GodMode9 only with the blue permissions(without any permissions if you are on 1.6.3 or below, MTmod 1.1 or below)

Each script is compatible with certain versions of GodMode9. __DO NOT USE WRONG VERSION__ or you may write to unexpected location in RAM and might(not impossible) **BRICK** your console.
Though you are probably safe on v1.4.0 and higher as they check the GodMode9's version and proceed only if it's compatible.

Use them at your own risk. I am NOT responsible for anything.

## How to use
1) Put the scripts somewhere under /gm9/scripts in your SD card.
2) If you are using version 1.3.1 or earlier, put the "res.bin" to the same location.
3) Launch GodMode9
4)
- If you are using 1.4.0 or later
    1) Press the home button.
    2) Select "scripts...".
    3) Go to directory where the scripts is placed. (if you put them to /gm9/scripts itself, nothing to do at this step)
    4) Select the version of GodMode9 you are using.
- If 1.3.1 or earlier(You can do this even you are using 1.4.0 or later)
    1) Select "[0:] SDCARD".
    2) Go to directory where the scripts is placed.
    3) Select the version of GodMode9 you are using.
    4) Select "Execute GM9 script"

## Requirement
Godmode9 version from 1.2.8 to 1.7.1 or MTmod from 1.0 to 1.2

## Credits
- [__d0k3__](https://github.com/d0k3) for [GodMode9](https://github.com/d0k3/GodMode9)
- Everyone who contributed to GodMode9
